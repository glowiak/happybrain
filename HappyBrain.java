import java.io.File; import java.io.FileReader; import java.io.BufferedReader; import java.io.IOException; import java.util.Scanner;
public class HappyBrain
{
    public static int[] reg = { 0, 0, 0 }; public static int selReg = 0;
    public static void main(String[] args)
    { if (args.length <= 0) { System.exit(1); }
        if (!new File(args[0]).exists()) { System.exit(1); }
        try { FileReader fr = new FileReader(args[0]); BufferedReader br = new BufferedReader(fr);
            String s = null;
            while ((s = br.readLine()) != null)
            {
                for (int i = 0; i < s.length(); i++)
                {
                    if (s.charAt(i) == 'a') { selReg = 0; }
                    if (s.charAt(i) == 'b') { selReg = 1; }
                    if (s.charAt(i) == 'c') { selReg = 2; }
                    if (s.charAt(i) == '+') { reg[selReg]++; }
                    if (s.charAt(i) == '-') { reg[selReg]--; }
                    if (s.charAt(i) == '*') { reg[selReg] = reg[selReg] * reg[1]; }
                    if (s.charAt(i) == '/') { reg[selReg] = reg[selReg]/reg[1]; }
                    if (s.charAt(i) == '.') { reg[selReg] = 0; }
                    if (s.charAt(i) == '>') { System.out.print(String.format("%c", (char)reg[selReg])); }
                    if (s.charAt(i) == '<') { reg[selReg] = (int)new Scanner(System.in).next().charAt(0); }
                }
            } br.close(); fr.close(); } catch (IOException ioe) { System.out.println(ioe); }}}

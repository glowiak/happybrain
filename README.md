# happybrain

A minimal esoteric programming language, basically a clone of brai...k.

It's written in Java (so bloated), for clarity the code has 25 lines, though it can be smaller by messing the clarity.

The .class file is very large, has 1.7 kilobytes. Sorry.

### Getting it

If you are just getting into the topic, download the prebuild [HappyBrain.class](https://codeberg.org/attachments/0683672b-ba38-431e-a75b-453db375f9bd) and run it with Java 8 or newer (must do from console, it's not a jar).

If you are advanced, download [HappyBrain.java](https://codeberg.org/glowiak/happybrain/raw/branch/master/HappyBrain.java) and compile it with javac.

If you are a real superior, rewrite this:

    import java.io.File; import java.io.FileReader; import java.io.BufferedReader; import java.io.IOException; import java.util.Scanner;
    public class HappyBrain
    {
        public static int[] reg = { 0, 0, 0 }; public static int selReg = 0;
        public static void main(String[] args)
        { if (args.length <= 0) { System.exit(1); }
            if (!new File(args[0]).exists()) { System.exit(1); }
            try { FileReader fr = new FileReader(args[0]); BufferedReader br = new BufferedReader(fr);
                String s = null;
                while ((s = br.readLine()) != null)
                {
                    for (int i = 0; i < s.length(); i++)
                    {
                        if (s.charAt(i) == 'a') { selReg = 0; }
                        if (s.charAt(i) == 'b') { selReg = 1; }
                        if (s.charAt(i) == 'c') { selReg = 2; }
                        if (s.charAt(i) == '+') { reg[selReg]++; }
                        if (s.charAt(i) == '-') { reg[selReg]--; }
                        if (s.charAt(i) == '*') { reg[selReg] = reg[selReg] * reg[1]; }
                        if (s.charAt(i) == '/') { reg[selReg] = reg[selReg]/reg[1]; }
                        if (s.charAt(i) == '.') { reg[selReg] = 0; }
                        if (s.charAt(i) == '>') { System.out.print(String.format("%c", (char)reg[selReg])); }
                        if (s.charAt(i) == '<') { reg[selReg] = (int)new Scanner(System.in).next().charAt(0); }
                    }
                } br.close(); fr.close(); } catch (IOException ioe) { System.out.println(ioe); }}}

And compile it.

### Instructions

HappyBrain has three integer registers - a, b,c. Mark it selected by just typing its name. Then you can manipulate the value with following symbols:

    +   | add 1
    -   | remove 1
    *   | reg*b
    /   | reg/b
    .   | set reg to 0
    >   | print the resulting ASCII character
    <   | read an ASCII character to selected register as an integer

### Hello World!

Exactly, it's very simple. The source file is [hello.hb](https://codeberg.org/glowiak/happybrain/raw/branch/master/hello.hb), but it's so small I'll paste it here:

    b++++++++++a+**---------------------------->.+**+>+++++++>>+++>.+*++++++++++++++++++++++>.+**------------->.+**+++++++++++>+++>------>.+**>.+*+++++++++++++++++++++++>.+*>

A oneliner. Run with 'java HappyBrain hello.hb'.